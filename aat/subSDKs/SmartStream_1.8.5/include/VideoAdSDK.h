
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#if __IPHONE_OS_VERSION_MAX_ALLOWED <= 60000

enum UIInterfaceOrientationMask : NSUInteger {
    UIInterfaceOrientationMaskPortrait = (1 << UIInterfaceOrientationPortrait),
    UIInterfaceOrientationMaskLandscapeLeft = (1 << UIInterfaceOrientationLandscapeLeft),
    UIInterfaceOrientationMaskLandscapeRight = (1 << UIInterfaceOrientationLandscapeRight),
    UIInterfaceOrientationMaskPortraitUpsideDown = (1 << UIInterfaceOrientationPortraitUpsideDown),
    UIInterfaceOrientationMaskLandscape = (UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight),
    UIInterfaceOrientationMaskAll = (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortraitUpsideDown),
    UIInterfaceOrientationMaskAllButUpsideDown = (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight),
    };
typedef enum UIInterfaceOrientationMask : NSUInteger UIInterfaceOrientationMask;

#endif

@protocol VideoAdSDKDelegate <NSObject>

@optional
- (void)advertisingIsPreparingToPlay;
- (void)advertisingIsReady;
- (void)advertisingWillShow;
- (void)advertisingDidHide;
- (void)advertisingClicked;
- (void)advertisingPrefetchingDidComplete;
- (void)advertisingPrefetchingDidCompleteWithAd:(BOOL)adAvailable;
- (void)advertisingNotAvailable;
- (void)advertisingFailedToLoad:(NSError*)error;
- (void)advertisingEventTracked:(NSString*)event;
- (void)advertisingIsAvailable;
@end

@interface VideoAdSDK : NSObject

@property (retain, nonatomic) id<VideoAdSDKDelegate> delegate;
@property (nonatomic) BOOL compatibleEnvironment;
@property (nonatomic) BOOL allowCellularPrefetching;

+ (VideoAdSDK*)sharedInstance;

+ (BOOL)registerWithPublisherID:(NSString*)publisherID delegate:(id<VideoAdSDKDelegate>)delegate;
+ (BOOL)playAdvertising;
+ (BOOL)prepareAdvertising;
+ (BOOL)prefetchAdvertising;
+ (void)removeCachedAds;

/**
 Use <tt>+[VideoAdSDK setSupportedInterfaceOrientationsMask:]</tt>.
 */
+ (void)setForceInterfaceOrientation:(BOOL)force DEPRECATED_ATTRIBUTE;

/**
 Use <tt>+[VideoAdSDK setSupportedInterfaceOrientationsMask:]</tt>.
 */
+ (void)setForcedInterfaceOrientation:(UIInterfaceOrientation)orientation DEPRECATED_ATTRIBUTE;

/**
 Allowed interface orientations for the ad. Default is <tt>UIInterfaceOrientationMaskAll</tt>.
 */
+ (void)setSupportedInterfaceOrientationsMask:(UIInterfaceOrientationMask)supportedInterfaceOrientationsMask;

+ (void)setUserAttribute:(NSString*)attribute forKey:(NSString*)key;
+ (void)setDelegate:(id<VideoAdSDKDelegate>)delegate;


@end
