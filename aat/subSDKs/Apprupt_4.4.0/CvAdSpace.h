//
//  CvAdSpace.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CvDefinitions.h"

@protocol CvAdSpaceDelegate;
@class CvAdSpaceOptions;

@interface CvAdSpace : NSObject

@property (nonatomic,readonly,strong) NSString * adSpaceId;
@property (nonatomic,readonly,strong) CvAdSpaceOptions * options;
@property (nonatomic,readonly) UIView * view;
@property (nonatomic,strong) id<CvAdSpaceDelegate> delegate;
@property (nonatomic,readonly) CGSize size;

- (BOOL) load;
- (void) prefetchAd:(CvAdPrefetchCallback)completion;
- (void) resume;
- (void) pause;
- (void) clear;
- (CGSize) sizeForWidth:(CGFloat)width;
- (CGSize) sizeForSize:(CGSize)size;




/*! @obsolete v3.7.3 */
@property (nonatomic,assign) NSUInteger cacheMaxSize __attribute__((unavailable("Removed in CvSDK 3.7.3")));
/*! @obsolete v3.7.3 */
@property (nonatomic,assign) BOOL keepCacheFull __attribute__((unavailable("Removed in CvSDK 3.7.3")));


/*! @obsolete v3.7.3 */
- (void) checkAdsAvailable:(CvAdsAvailableCallback)completion __attribute__((unavailable("Removed in CvSDK 3.7.3")));

@end
