//
//  CvInterstitialDelegate.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CvInterstitialDelegate <NSObject>

@optional
- (void) cvInterstitialWillAppear:(NSString *)adSpaceId;
- (void) cvInterstitialDidAppear:(NSString *)adSpaceId;
- (void) cvInterstitialWillDisappear:(NSString *)adSpaceId;
- (void) cvInterstitialDidDisappear:(NSString *)adSpaceId;
- (void) cvInterstitialDidFailWithError:(NSError *)error;
- (void) cvInterstitialDidReceiveFirstTap:(NSString *)adSpaceId;

@end