//
//  CvInterstitialPresenter.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @obsolete v3.6.0 */
__attribute__((unavailable("Removed in CvSDK 3.6.0")))
@protocol CvInterstitialPresenter <NSObject>
@optional
- (void) presentCvInterstitialModally:(UIViewController *)cvInterstitial;
- (void) dismissCvInterstitial:(UIViewController *)cvInterstitial;
@end
