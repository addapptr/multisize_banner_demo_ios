//
//  CvSDK_AdColony.h
//  CvSDK+AdColony
//
//  Created by Michal Kluszewski on 20/10/15.
//  Copyright © 2015 Opera Mediaworks GmbH. All rights reserved.
//

extern NSString * const kCvAdColony;
extern NSString * const kCvAdColony_NativeAdTextColor;
extern NSString * const kCvAdColony_NativeAdBackgroundColor;
extern NSString * const kCvAdColony_NativeAdSponsoredTextColor;
extern NSString * const kCvAdColony_NativeAdSponsoredText;
extern NSString * const kCvAdColony_NativeAdButtonTextColor;

