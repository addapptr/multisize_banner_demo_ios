//
//  CvAdSpaceOptions.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CvDefinitions.h"

@class CvExtraParams;

@interface CvAdSpaceOptions : NSObject

@property (nonatomic,strong) NSString * categories;
@property (nonatomic,strong) NSString * keywords;
@property (nonatomic,assign) CvAdSpaceAnimationType animationType;
@property (nonatomic,assign) NSTimeInterval minRequestsInterval;
@property (nonatomic,assign) BOOL clearSpaceBeforeLoad;
@property (nonatomic,strong,readonly) CvExtraParams *extraParams;
@property (nonatomic,strong,readonly) NSMutableDictionary *mediationSettings;

@end