//
//  CvDefinitions.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kCvI18N_Call;
extern NSString * const kCvI18N_Cancel;
extern NSString * const kCvI18N_OK;
extern NSString * const kCvI18N_TakePhoto;
extern NSString * const kCvI18N_RecordVideo;
extern NSString * const kCvI18N_ChooseFromLibrary;
extern NSString * const kCvI18N_UseFrontCamera;
extern NSString * const kCvI18N_UseRearCamera;
extern NSString * const kCvI18N_EventAddedTitle;
extern NSString * const kCvI18N_EventAdded;
extern NSString * const kCvI18N_EventsAdded;
extern NSString * const kCvI18N_ReminderAddedTitle;
extern NSString * const kCvI18N_ReminderAdded;
extern NSString * const kCvI18N_RemindersAdded;




// mediation
extern NSString * const kCvMediationOrder;

extern NSString * const kCvApprupt;

typedef NS_OPTIONS(NSUInteger, CvSDKDebugLevel) {
    CvSDKDebugLevelQuiet    = 0,
    CvSDKDebugLevelErrors   = 1,
    CvSDKDebugLevelWarnings = 3,
    CvSDKDebugLevelInfo     = 7,
    CvSDKDebugLevelVerbose  = 15,
    
    CvSDKDebugLevelAll      = 15,
};


typedef NS_ENUM(NSUInteger, CvAdSpaceAnimationType) {
    CvAdSpaceAnimationTypeDefault,
    CvAdSpaceAnimationTypeNone,
    CvAdSpaceAnimationTypeLeftRight,
    CvAdSpaceAnimationTypeRightLeft,
    CvAdSpaceAnimationTypeTopBottom,
    CvAdSpaceAnimationTypeBottomTop,
    CvAdSpaceAnimationTypeFade
};


typedef NS_ENUM(NSUInteger, CvGender) {
    CvGenderUnknown,
    CvGenderMale,
    CvGenderFemale
};

typedef NS_ENUM(NSUInteger, CvRelationshipStatus) {
    CvRelationshipStatusUnknown,
    CvRelationshipStatusSingle,
    CvRelationshipStatusInRelationship,
    CvRelationshipStatusEngaged,
    CvRelationshipStatusMarried,
    CvRelationshipStatusSeparated,
    CvRelationshipStatusDivorced,
    CvRelationshipStatusWidowed
};

typedef void (^CvAdsAvailableCallback)(BOOL available);
typedef void (^CvAdPrefetchCallback)(BOOL success, NSError *error);