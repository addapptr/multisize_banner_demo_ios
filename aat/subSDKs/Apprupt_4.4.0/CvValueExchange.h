//
//  CvValueExchange.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CvValueExchangeDelegate;
@protocol CvInterstitialDelegate;

@interface CvValueExchange : NSObject

@property (nonatomic,weak) id<CvValueExchangeDelegate> delegate;

- (void) presentVEAd:(NSString *)adSpaceId;
- (void) presentVEAd:(NSString *)adSpaceId withDelegate:(id<CvInterstitialDelegate>)delegate;
- (BOOL) isVEAvailableForAdSpace:(NSString *)adSpaceId;
- (NSString *)currencyNameForAdSpace:(NSString *)adSpaceId;
- (NSInteger)rewardAmounForAdSpace:(NSString *)adSpaceId;
- (NSInteger)veImpressionsLeftForAdSpace:(NSString *)adSpaceId;

@end