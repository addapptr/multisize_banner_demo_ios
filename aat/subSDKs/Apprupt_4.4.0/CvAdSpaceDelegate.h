//
//  CvAdSpaceDelegate.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CvAdSpace;

@protocol CvAdSpaceDelegate <NSObject>
@required
- (CGRect) cvAdSpace:(CvAdSpace *)adSpace willChangeViewSize:(CGSize)newSize;
@optional
- (void) cvAdSpaceWillShowOverlay:(CvAdSpace *)adSpace;
- (void) cvAdSpaceDidShowOverlay:(CvAdSpace *)adSpace;
- (void) cvAdSpaceWillHideOverlay:(CvAdSpace *)adSpace;
- (void) cvAdSpaceDidHideOverlay:(CvAdSpace *)adSpace;
- (void) cvAdSpaceLoadedFirstAd:(CvAdSpace *)adSpace;
- (void) cvAdSpaceLoadedFirstAd:(CvAdSpace *)adSpace withSize:(CGSize)size;
- (void) cvAdSpaceLoadedAd:(CvAdSpace *)adSpace;
- (void) cvAdSpaceLoadedAd:(CvAdSpace *)adSpace withSize:(CGSize)size;
- (void) cvAdSpaceAdDidAppear:(CvAdSpace *)adSpace first:(BOOL)first;
- (void) cvAdSpace:(CvAdSpace *)adSpace failedWithError:(NSError *)error;
- (void) cvAdSpaceDidReceiveFirstTap:(CvAdSpace *)adSpace;


/*! @obsolete v3.6.1 */
- (void) presentCvLandingPageControllerModally:(UIViewController *)controller __attribute__((unavailable("Removed in CvSDK 3.6.1")));

/*! @obsolete v3.6.1 */
- (void) dismissCvLandingPageController:(UIViewController *)controller __attribute__((unavailable("Removed in CvSDK 3.6.1")));

@end
