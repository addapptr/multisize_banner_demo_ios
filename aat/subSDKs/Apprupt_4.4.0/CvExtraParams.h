//
//  CvSDKLiteExtras.h
//  CvSDKLite
//
//  Created by Michał Kluszewski on 02/12/14.
//  Copyright (c) 2014 apprupt GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CvExtraParams : NSObject

- (void) setValue:(NSString *)value forKey:(NSString *)key;
- (id) valueForKey:(NSString *)key;
- (void) addEntriesFromDictionary:(NSDictionary *)values;
- (void) removeAllObjects;
- (void) removeObjectForKey:(NSString *)key;
- (void) removeObjectsForKeys:(NSArray *)keys;
- (id) objectForKey:(id)key;
- (id) objectForKeyedSubscript:(id)key;
- (void) setObject:(id)value forKey:(id<NSCopying>)aKey;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;


@end
