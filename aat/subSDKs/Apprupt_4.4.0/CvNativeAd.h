//
//  CvNativeAd.h
//  CvSDK
//
//  Created by Michal Kluszewski on 22/09/15.
//  Copyright © 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CvNativeAdAssetType) {
    CvNativeAdAssetTypeImage,
    CvNativeAdAssetTypeVideo
};


@interface CvNativeAdAsset : NSObject

@property (nonatomic,assign,readonly) CvNativeAdAssetType type;
@property (nonatomic,assign,readonly) CGSize size;
@property (nonatomic,assign,readonly) NSTimeInterval duration;
@property (nonatomic,strong,readonly) NSURL *url;

@end


@interface CvNativeAdLabel : CvNativeAdAsset

@property (nonatomic,strong,readonly) NSURL *targetURL;

@end

@interface CvNativeAdRating : NSObject

@property (nonatomic,assign,readonly) CGFloat value;
@property (nonatomic,assign,readonly) CGFloat scale;

@end


@interface CvNativeAd : NSObject

@property (nonatomic,readonly) NSString *identifier;
@property (nonatomic,readonly) NSString *title;
@property (nonatomic,readonly) NSString *shortItemDescription;
@property (nonatomic,readonly) NSString *itemDescription;
@property (nonatomic,readonly) NSString *socialContext;
@property (nonatomic,readonly) NSURL *callToActionURL;
@property (nonatomic,readonly) NSString *callToActionString;
@property (nonatomic,readonly) CvNativeAdAsset *icon;
@property (nonatomic,readonly) CvNativeAdAsset *keyVisual;
@property (nonatomic,readonly) CvNativeAdLabel *adLabel;
@property (nonatomic,readonly) CvNativeAdRating *rating;
@property (nonatomic,readonly) NSArray *aiTrackingURLs;
@property (nonatomic,readonly) NSString *aiTrackingHTML;


- (void) trackImpression;

@end
