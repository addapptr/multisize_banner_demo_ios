//
//  CvSDK.h
//
//  Created by Michał Kluszewski on 11.05.2012.
//  Copyright (c) 2012 apprupt GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>



#import "CvDefinitions.h"
#import "CvInterstitialDelegate.h"
#import "CvInterstitialPresenter.h"
#import "CvAdSpaceDelegate.h"
#import "CvExtraParams.h"
#import "CvAudience.h"
#import "CvAdSpaceOptions.h"
#import "CvMediationSettings.h"
#import "CvAdSpace.h"
#import "CvValueExchangeDelegate.h"
#import "CvValueExchange.h"
#import "CvNativeAd.h"
#import "CvSDK+AdColony.h"


@interface CvSDK : NSObject

@property (nonatomic,readonly,strong) NSString * piKey;
@property (nonatomic,readonly,strong) CvAdSpaceOptions * defaultOptions;
@property (nonatomic,readonly,strong) CvAudience *audience;
@property (nonatomic,assign) BOOL useSecureConnection;
@property (nonatomic,readonly) CvMediationSettings *mediationSettings;
@property (nonatomic,readonly) CvValueExchange *valueExchange;


+ (CvSDK *) sharedInstance;
+ (NSString *) version;
+ (void) setDebugLevel:(CvSDKDebugLevel)level;
- (void) start;
- (void) regeneratePiKey;
- (void) enableGeoLocation;
- (void) disableGeoLocation;
- (void) enableHeadingMonitor;
- (void) disableHeadingMonitor;
- (void) updateGeoLocation:(CLLocation *)location;
- (void) updateHeading:(CLHeading *)heading;
- (CvAdSpace *) createAdSpace:(NSString *)adSpaceId withDelegate:(id<CvAdSpaceDelegate>)delegate;
- (CvAdSpace *) createAdSpace:(NSString *)adSpaceId withDelegate:(id<CvAdSpaceDelegate>)delegate andViewFrame:(CGRect)frame;
- (CvAdSpace *) retrieveAdSpace:(NSString *)adSpaceId;
- (void) startInterstitial:(NSString *)adSpaceId;
- (void) startInterstitial:(NSString *)adSpaceId withDelegate:(id<CvInterstitialDelegate>)delegate;
- (void) prefetchInterstitial:(NSString *)adSpaceId completion:(CvAdPrefetchCallback)completion;
- (void) pauseAdSpaces:(NSArray *)spaces;
- (void) resumeAdSpaces:(NSArray *)spaces;
- (void) destroyAdSpace:(NSString *)adSpaceId;
- (void) destroyAdSpaces:(NSArray *)spaces;
- (void) forI18NString:(NSString *)i18nString setTranslation:(NSString *)translation;
- (void) loadNativeAdForAdSpace:(NSString *)adSpaceId completion:(void(^)(NSError *, CvNativeAd *))completion;
+ (NSString *) i18n:(NSString *)i18nString;


/*! @obsolete v3.6.0 */
- (void) startInterstitial:(NSString *)adSpaceId withHostController:(UIViewController<CvInterstitialPresenter> *)controller __attribute__((unavailable("Removed in CvSDK 3.6.0, use startInsterstitial: instead")));

/*! @obsolete v3.6.0 */
- (void) startInterstitial:(NSString *)adSpaceId withHostController:(UIViewController<CvInterstitialPresenter> *)controller andDelegate:(id<CvInterstitialDelegate>)delegate __attribute__((unavailable("Removed in CvSDK 3.6.0, use startInsterstitial:withDelegate: instead")));

/*! @obsolete v3.7.3 */
- (void) checkInterstitialAvailable:(NSString *)adSpaceId completion:(CvAdsAvailableCallback)completion __attribute__((unavailable("Removed in CvSDK 3.7.3")));
@end
