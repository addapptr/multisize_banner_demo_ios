//
//  CvValueExchangeDelegate.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CvValueExchangeDelegate <NSObject>

- (void) cvVERewardForAdSpace:(NSString *)adSpaceId amount:(NSInteger)amount currencyName:(NSString *)currencyName;

@end