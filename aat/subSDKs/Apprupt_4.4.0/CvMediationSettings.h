//
//  CvMediationSettings.h
//  CvSDK
//
//  Created by Michał Kluszewski on 05/05/15.
//  Copyright (c) 2015 apprupt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CvMediationSettings : NSObject

- (NSMutableDictionary *)globalSettings;
- (NSMutableDictionary *)settingsForAdSpace:(NSString *)adSpace;

@end
