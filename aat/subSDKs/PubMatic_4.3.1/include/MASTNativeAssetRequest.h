/*
 
 * PubMatic Inc. ("PubMatic") CONFIDENTIAL
 
 * Unpublished Copyright (c) 2006-2016 PubMatic, All Rights Reserved.
 
 *
 
 * NOTICE:  All information contained herein is, and remains the property of PubMatic. The intellectual and technical concepts contained
 
 * herein are proprietary to PubMatic and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 
 * from PubMatic.  Access to the source code contained herein is hereby forbidden to anyone except current PubMatic employees, managers or contractors who have executed
 
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 
 *
 
 * The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
 
 * information that is confidential and/or proprietary, and is a trade secret, of  PubMatic.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
 
 * OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF PubMatic IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
 
 * LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 
 * TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
 
 */


//
//  MASTNativeAsset.h
//  MASTAdView
//

#import <Foundation/Foundation.h>

@interface MASTNativeAssetRequest : NSObject

/*!
 @property
 @abstract Unique asset ID, assigned by exchange.
 */
@property(nonatomic,assign) NSInteger assetId;

/*!
 @property
 @abstract Set to 1 if the asset is required. Default value is 0 (optional).
 */
@property(nonatomic,assign) NSInteger required;

/*!
 @property
 @abstract Asset type. Possible values: “title”, “img”, “data”. Its value is automatically set on creating specific instance of derived class
 */
@property(nonatomic,strong) NSString *type;

/*!
 @property
 @abstract Set to 1 if the asset is required. Default value is 0 (optional).
 */
@property(nonatomic,strong) NSMutableDictionary* entityDictionary;

-(NSMutableDictionary *) getJSONDictionary;

@end
