//
//  MASTDictionary.h
//  MASTAdView
//
//  Created by Prabhakar on 23/06/15.
//  Copyright (c) 2015 Mocean Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#define DELIMITER @"|"

@interface MASTDictionary : NSObject
{
    NSMutableDictionary *adRequestDictionary;
}
- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey;
- (void)setValue:(id)value forKey:(NSString *)key;
- (id)valueForKey:(id)key;
- (void)removeObjectsForKeys:(NSArray *)keyArray;
-(NSDictionary*)dictionary;
- (id)objectForKey:(id)aKey;
@end
