//
//  AATKit.h
//
//  Created by Daniel Brockhaus on 04.04.12.
//  Copyright (c) 2012-2013 AddApptr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef AATKITDELEGATE
#define AATKITDELEGATE
/**
 * The AATKit delegate protocol.
 *
 * The `AATKitDelegate` protocol defines methods that are called by the
 * AATKit framework in response to important events.
 */
@protocol AATKitDelegate<NSObject>

@optional

/// @name All Placement Types

/**
 * A placement is ready to display an ad.
 *
 * This method will be called for banner, full screen, and native
 * placements.
 * - Banner placements will automatically display the new ad.
 * - Full screen placements can now invoke +[AATKit showPlacement:].
 * - Native placements can now invoke +[AATKit getNativeAdForPlacement:].
 *
 * Because of the delayed and asynchronous nature of the ad networks'
 * responses, it is possible to get multiple notifications for a single call
 * to +[AATKit reloadPlacement:]. Use +[AATKit haveAdForPlacement:] to check
 * for these cached responses before calling +[AATKit reloadPlacement:]
 * again.
 *
 * **NOTE:** This method will not be called when displaying a banner from
 * the internal EMPTY network. Use -[AATKitDelegate AATKitShowingEmpty:]
 * to receive notifications for these banners.
 *
 * @see +[AATKit showPlacement:]
 * @see +[AATKit getNativeAdForPlacement:]
 * @see -[id\<AATKitDelegate> AATKitShowingEmpty:]
 */
- (void) AATKitHaveAd:(nonnull id) placement;


/**
 * Multi-Size placement specific callback.
 * When a placement of type AATKitBannerMultiSize is ready to display an ad.
 * Since multi-size banner views vary in size, the reference to a new UIView
 * instance is provided in the callback as well. 
 * 
 * *NOTE* The reference to the multi-size banner view (placementView) is autorelease.
 * Hence the receiver has to make sure to retain the the UIView instance himself,
 * as well as dispose the old UIView instance if present.
 * 
 * The callback AATKitHaveAd will not be called for multi-size banner placements.
 */
- (void) AATKitHaveAdOnMultiSizeBanner:(nonnull id) placement withView:(nonnull UIView*) placementView;

/**
 * A placement could not get ready to display an ad.
 *
 * This method will be called when AATKit failed to load an ad within a
 * predefined time interval.
 *
 * This method is a good place to hide banner ads that don't have any
 * content.
 */
- (void) AATKitNoAds:(nonnull id) placement;

/// @name Banner Placements

/**
 * An empty banner is ready to be displayed.
 *
 * This method will be called for banner placements when it will display a
 * banner ad from the internal EMPTY network. The EMPTY network displays a
 * transparent banner.
 *
 * If a banner from a different network will be loaded, -[AATKitDelegate
 * AATKitHaveAd:] will be called.
 *
 * @see -[id\<AATKitDelegate> AATKitHaveAd:]
 */
- (void) AATKitShowingEmpty:(nonnull id) placement;

/// @name Full Screen Placements

/**
 * A full screen ad will be displayed.
 *
 * In this callback, you can e.g. suspend a game while the user is watching
 * a full screen ad.
 *
 * *NOTE:* This callback is unreliable due to inconsistent use of callbacks
 * in different ad network SDKs.
 *
 * @see -[id\<AATKitDelegate> AATKitResumeAfterAd]
 */
- (void) AATKitPauseForAd;

/**
 * A full screen ad was displayed.
 *
 * In this callback, you can e.g. resume a game after the user was watching
 * a full screen ad.
 *
 * *NOTE:* This callback is unreliable due to inconsistent use of callbacks
 * in different ad network SDKs.
 *
 * @see -[id\<AATKitDelegate> AATKitPauseForAd]
 */
- (void) AATKitResumeAfterAd;

/// @name Other Methods

/**
 * Please ignore unless told otherwise by AddApptr support.
 */
- (bool) AATKitAppWillHandleURL:(nonnull NSURLRequest*) url;

/**
 * Please ignore unless told otherwise by AddApptr support.
 */
- (void) AATKitUserEarnedIncentive;

- (void) AATKitUserEarnedIncentiveOnPlacement: (nonnull id) placement;
- (void) AATKitObtainedAdRules: (bool) fromTheServer;
@end
#endif

#ifndef AATKITBANNERALIGN
#define AATKITBANNERALIGN
/**
 * Banner Alignment Constants
 *
 * Sometimes different ad networks have very similar ad sizes. When this
 * happens, the banner container will get a size allocation fitting the
 * largest of these banner types.
 *
 * To make sure smaller banners will get properly aligned, use these
 * constants for `+[AATKit setPlacementAlign:forPlacement:]` to provide a
 * hint about the desired banner alignment.
 *
 * @see +[AATKit setPlacementAlign:forPlacement:]
 */
typedef NS_ENUM(NSInteger, AATKitBannerAlign) {
    /**
     * Banners should be aligned to the top edge of the container view.
     * This alignment is recommended for banner ads at the top of the
     * screen.
     */
    AATKitBannerTop,
    /**
     * Banners should be aligned to the bottom edge of the container view.
     * This alignment is recommended for banner ads at the bottom of the
     * screen.
     */
    AATKitBannerBottom,
    /**
     * Banners should be vertically centered in the container view. This
     * Alignment is recommended for banner ads embedded in scrollable
     * content, such as instances of `UITableViewCell`.
     */
    AATKitBannerCenter
};
#endif

#ifndef AATKITADTYPE
#define AATKITADTYPE
/**
 * Ad Placement Types.
 *
 * *NOTE:* Not all types are supported by all ad networks.
 */
typedef NS_ENUM(NSInteger, AATKitAdType) {
    AATKitBanner320x53,
    /// An iPhone 6 wrapper for the 320x53 banner size.
    AATKitBanner375x53,
    /// An iPhone 6 Plus wrapper for the 320x53 banner size.
    AATKitBanner414x53,
    /// A banner size for iPad devices.
    AATKitBanner768x90,
    /// The standard “medium rectangle”
    AATKitBanner300x250,
    /// A banner size fitting a landscape iPhone.
    AATKitBanner468x60,
    /// A multi size banner comprising banner of different sizes
    AATKitBannerMultiSize,
    /// Full Screen Ads
    AATKitFullscreen,
    /// Native Ads
    AATKitNativeAd,
    /// (Only available from iAd.)
    AATKitBanner480x32,
    /// (Only available from iAd.)
    AATKitBanner1024x66,
    /// (Only available from House Ad.)
    AATKitBanner300x200,
    /// (Only available from House Ad.)
    AATKitBanner90x80,
    /// (Only available from House Ad.)
    AATKitBanner200x200
};
#endif

/**
 * Ad Networks
 *
 * You can use these enum values to enable or disable ad networks.
 *
 * @see +[AATKit reenableAdNetwork:]
 * @see +[AATKit disableAdNetwork:]
 */
typedef NS_ENUM(NSInteger, AATKitAdNetwork) {
    AATAdColony,
    AATAdMob,
    AATAddapptr,
    AATAdX,
    AATAmazon,
    AATAppLift,
    AATApplovin,
    AATApprupt,
    AATDFP,
    AATFacebook,
    AATFlurry,
    AATGroupM,
    AATHouse,
    AATHouseX,
    AATiAd,
    AATInmobi,
    AATLoopMe,
    AATMadvertise,
    AATMdotM,
    AATMillennial,
    AATMobFox,
    AATMoPub,
    AATMock,
    AATPlayhaven,
    AATNexage,
    AATSmaato,
    AATSmartAd,
    AATSmartClip,
    AATSmartStream,
    AATUnity,
    AATPubMatic,
    AATPermodo,
    AATUnknownNetwork
};

/**
 * Native Ad Content Types
 *
 * This type is used to indicate the content type of a native ad.
 */
typedef NS_ENUM(NSInteger, AATKitNativeAdType) {
    /// The user will be prompted to install an application.
    AATKitNativeAppInstall,
    /// Undocumented
    AATKitNativeContent,
    AATKitNativeVideo,
    AATKitNativeOther,
    AATKitNativeTypeUnknown
};

#ifndef AATKITCLASS
#define AATKITCLASS
/**
 * AATKit Public Interface
 *
 * The `AATKit` class is used to communicate with the framework. It is the
 * main interface for you to configure and display ads.
 *
 * Using AATKit requires four steps in general:
 *
 * 1. Initialize AATKit:
 *   * `+[AATKit initWithViewController:andDelegate:]`
 * 2. Create a placement:
 *   * `+[AATKit createPlacementWithName:andType:]`
 * 3. Load ads for the placement:
 *   * `+[AATKit reloadPlacement:]`
 *   * `+[AATKit startPlacementAutoLoad:]`
 * 4. Display the ad:
 *   * `+[AATKit getPlacementView:]` (for banner ads)
 *   * `+[AATKit showPlacement:]` (for full screen ads)
 *   * `+[AATKit getNativeAdForPlacement:]` (for native ads)
 */
// FIXME: Provide some example snippets for the different kinds of ads.
@interface AATKit : NSObject

/// @name Initialization and Configuration

/**
 * Initialize AATKit.
 *
 * This method call must be the first call into AATKit. It will initialize
 * the framework and trigger the initial download of rules from the server.
 *
 * @param viewController The view controller that will be used to present
 * resp. dismiss other view controllers. You can update the view controller
 * using -setViewController: later. May be `nil`.
 *
 * @param delegate The delegate to be notified when important events occur.
 * May be `nil`.
 */
+ (void) initWithViewController: (nullable UIViewController*)viewController andDelegate: (nullable id<AATKitDelegate>) delegate;

/**
 * Initialize AATKit for Development.
 *
 * As it takes some time to establish new accounts and their connections to
 * ad networks, there won't be real ads for you right from the start.
 *
 * To simplify the integration of the SDK, you can enable the test mode to
 * provide you with real ads for testing.
 *
 * @param myID The test ID from your welcome email.
 */
+ (void) initWithViewController: (nullable UIViewController*)viewcon delegate: (nullable id<AATKitDelegate>) delegate andEnableTestModeWithID: (int) myID;

/**
 * Initialize AATKit with Initial Rules.
 *
 * This method allows to specify the rules defining the way AATKit works.
 * Using this method, you can avoid contacting the AddApptr servers. This
 * can improve the speed of loading the first ads.
 *
 * Please contact the support team for help when defining the initial rule
 * set.
 *
 * @param rulesCaching Specify whether the initial rule set should be
 * cached.
 *
 * **NOTE:** Please contact the AddApptr support team in case you want to
 * use this method.
 */
+ (void) initWithViewController: (nullable UIViewController*)viewcon delegate: (nullable id<AATKitDelegate>) delegate enableRulesCaching: (bool) rulesCaching andInitialRules: (nullable NSString*) initialRules;

/**
 * Specify the presenting view controller.
 *
 * Update the view controller that will be used by the framework to present
 * full screen view controllers. Full screen view controllers can be
 * presented by full screen ads (while they are being displayed) or even
 * banner ads (after the user tapped an ad).
 */
+ (void) setViewController:(nullable UIViewController*)con;

/**
 * Specify the delegate for AATKit.
 *
 * You can use this method to update the delegate object for AATKit.
 *
 * *NOTE:* Be careful when updating the delegate. You might still receive
 * notifications for previously used placements.
 */
+ (void) setDelegate:(nullable id<AATKitDelegate>)delegate;

/// @name Debugging Information

/**
 * Get the AATKit version number.
 *
 * Get the string representation of the AATKit version number.
 */
+ (nonnull NSString *) getVersion;

/**
 * Enable debugging.
 *
 * When debugging is enabled (`YES`), there will be more information printed
 * to the console.
 *
 * *Default:* `NO`.
 */
+ (void) debug:(bool) flag;

/**
 * Enable extended debugging.
 *
 * Enable extended debugging for +[AATKit setPlacementPos:forPlacement:].
 * Extended debugging checks whether the placement is manipulated from
 * -[UIViewController viewWillAppear:]. Updating the placement before
 * presenting the view controller's view can render an application unstable.
 *
 * **NOTE:** Only disable when you know what you're doing.
 *
 * *Default:* `YES`.
 *
 * @see +[AATKit setPlacementPos:forPlacement:]
 */
+ (void) extendedDebug: (bool) yesOrNo;

/**
 * Enable Shake-to-Debug.
 *
 * By default, AATKit will display a popup box with information about the
 * framework version and the currently visible ads. Use `[AATKit debugShake:NO]`
 * to disable this behavior.
 *
 * *Default:* `YES`.
 */
+ (void) debugShake:(bool) flag; //default is set to YES

/**
 * Collect debug information about AATKit.
 */
+ (nonnull NSString*) getDebugInfo;

/// @name Placement Management

/**
 * Create a new Placement.
 *
 * @param type Specifies the type of the placement. Banner placements are
 * treated differently from full screen placements.
 *
 * *NOTE:* You should register your `placementName` on the AddApptr website
 * if you intend to target it with specific rules. You must not use the
 * names `"banner"`, `"fullscreen"`, and `"promo"`.
 */
+ (nonnull id) createPlacementWithName:(nonnull NSString *) placementName andType:(AATKitAdType)type NS_RETURNS_RETAINED;

/**
 * Obtain a reference to a previously created placement.
 *
 * This method can be used to obtain a reference to a previously defined
 * placement.
 */
+ (nullable id) getPlacementWithName:(nonnull NSString *) placementName;

/**
 * Start automatic (re)loading of ads for a certain placement.
 *
 * Use `+[AATKit stopPlacementAutoReload:]` to restore the manual reload
 * mode.
 *
 * **For Banner Placements:**
 *
 * Use this method if you don't want to manually reload the ad of a banner
 * placement. This method will automatically reload banners every 30
 * seconds.
 *
 * Use `+[AATKit startPlacementAutoReloadWithSeconds:forPlacement:]` to
 * specify a custom time interval.
 *
 * **For Full Screen Placements:**
 *
 * Use this method to automatically load a new full screen ad while the
 * current one is being displayed.
 *
 * *NOTE:* You should not use both this method and `+[AATKit
 * reloadPlacement:]`.
 *
 * @see +[AATKit startPlacementAutoReloadWithSeconds:forPlacement:]
 * @see +[AATKit stopPlacementAutoReload:]
 * @see +[AATKit reloadPlacement:]
 */
+ (void) startPlacementAutoReload:(nonnull id) placement;

/**
 * Disable automatic reloading for a placement.
 */
+ (void) stopPlacementAutoReload:(nonnull id) placement;

/**
 * Set the parent view controller for a specific placement.
 *
 * This setting (if used) will override the global view controller. To
 * restore the global behavior, pass `nil` as an argument.
 */
+ (void) setPlacementViewController:(nullable UIViewController*)con forPlacement:(nonnull id) placement;

/**
 * Load/reload the content of a placement.
 *
 * After a banner placement was created, no ad was downloaded to be
 * displayed. Use this method to download the initial content for a
 * placement.
 *
 * Banner placements can reload their content by calling this method again.
 * The delegate will be notified about the reload result.
 *
 * Full screen placements need this method to download the full screen ad
 * before it is supposed to be displayed. The delegate will be notified
 * about the reload result.
 *
 * After using this method of these three delegate methods will be invoked:
 * - -[AATKitDelegate AATKitHaveAd:]
 * - -[AATKitDelegate AATKitNoAds:]
 * - -[AATKitDelegate AATKitShowEmpty:]
 */
+ (bool) reloadPlacement:(nonnull id) placement;

/**
 * Load/reload the content of a placement.
 *
 * After a banner placement was created, no ad was downloaded to be
 * displayed. Use this method to download the initial content for a
 * placement.
 *
 * Banner placements can reload their content by calling this method again.
 * The delegate will be notified about the reload result.
 *
 * Full screen placements need this method to download the full screen ad
 * before it is supposed to be displayed. The delegate will be notified
 * about the reload result.
 *
 * After using this method of these three delegate methods will be invoked:
 * - -[AATKitDelegate AATKitHaveAd:]
 * - -[AATKitDelegate AATKitNoAds:]
 * - -[AATKitDelegate AATKitShowEmpty:]
 *
 * @param forceLoad If `YES`, the placement will be reloaded even if other
 * requirements like the minimum display time are not met.
 */
+ (bool) reloadPlacement:(nonnull id) placement forceLoad:(bool) forceLoad;

/**
 * This method is not supported anymore.
 */
+ (void) setPlacementSubID:(int) subID forPlacement:(nonnull id) placement __deprecated_msg("Placement sub IDs are not supported anymore.");

/**
 * Query the current status of a placement.
 *
 * Use this call to find out whether a placement has an ad ready.
 *
 * *NOTE:* It is recommended to use the AATKitDelegate protocol to receive
 * updates for the placement status.
 */
+ (bool) haveAdForPlacement:(nonnull id) placement;

/// @name Banner Advertisements

/**
 * Start reloading placements automatically with a customized time interval.
 *
 * If you do not want to manually reload banner ads, you can use this method
 * to specify an interval for automatic reloading of banner ads.
 *
 *
 * *NOTE:* You should not mix this call with `+[AATKit reloadPlacement:]`.
 *
 * @see +[AATKit stopPlacementAutoReload:]
 */
+ (void) startPlacementAutoReloadWithSeconds:(int) seconds forPlacement:(nonnull id) placement;

/**
 * Get the banner container view.
 *
 * Returns the UIView containing the banner placement's content.
 *
 * *NOTE:* This view will be transparent (ans thus: invisible) until either
 * `+[AATKit reloadPlacement:]` or `+[AATKit startPlacementAutoReload:]`
 * have been called.
 *
 * To show the banner, you will have to add this view to your view
 * hierarchy. You might also need to bring the view to the foreground.
 */
+ (nullable UIView*) getPlacementView:(nonnull id) placement;

/**
 * Get the required size of the content.
 *
 * You can use this size to manually calculate the position of the view or
 * to set up auto-layout constraints.
 */
+ (CGSize) getPlacementContentSize:(nonnull id) placement;

/**
 * Specify the vertical alignment of the banner within the container view.
 *
 * Some of the “standard” banner ad formats are actually not the same for
 * all ad networks. E.g. some networks provide 320x48 sized banners for the
 * 320x50 category, some provide 320x53. AATKit allows for these differences
 * by creating a view that will fit the largest banner size (in this case,
 * 320x53). Smaller ads can be aligned to the top, the bottom or the middle
 * of the container view.
 */
+ (void) setPlacementAlign:(AATKitBannerAlign) align forPlacement:(nonnull id) placement;

/**
 * Define the top-left position of the banner container.
 */
+ (void) setPlacementPos:(CGPoint) pos forPlacement:(nonnull id) placement;

/**
 * Provide a default image for banner ads.
 *
 * Using this call, a banner placement can display a static image when no
 * ad could be loaded. The image will be scaled to fill the container view.
 * Use `nil` to revert to the default behavior.
 */
+ (void) setPlacementDefaultImage:(nullable UIImage *) image forPlacement:(nonnull id) placement;

/// @name Full Screen Advertizements

/**
 * Display a full screen placement.
 */
+ (bool) showPlacement:(nonnull id) placement;

/// @name Native Advertizements

/**
 * Describe the Rating of the Advertized Product.
 *
 * This can be used for native ads that are advertizing for other apps. Your
 * app can use this e.g. to display a star rating.
 */
struct AATKitNativeAdRating {
    /// The rating of the app, e.g. `3.5`.
    float value;
    /// The Scale for the rating, usually something like `5.0`
    float scale;
    /**
     * Indicates whether the ad network supports rating at all.
     *
     * *NOTE:* Just because this value is `YES`, does not mean that there is
     * useful data here.
     */
    bool hasRating;
};

// native placement lifecycle methods

/**
 * Query the number of ad requests that are pending on a placement.
 */
+ (NSUInteger) currentlyLoadingNativeAdsOnPlacement:(nonnull id) placement;

/**
 * Get the native ad object for a placement.
 *
 * This method is usually called in implementations of
 * `-[id\<AATKitDelegate> AATKitHaveAd:]`.
 */
+ (nullable id) getNativeAdForPlacement: (nonnull id) placement;

/**
 * Call this method when a native ad gets displayed for the first time.
 * This will make sure that the ad space will be correctly reported in the
 * AddApptr dashboard.
 */
+ (void) appHasAdSpaceForNativePlacement: (nonnull id) placement;

/**
 * Call this method before trying to call `+[AATKit reloadPlacement:]` to
 * avoid requesting native ads when such a request will fail anyway.
 */
+ (bool) isFrequencyCapReachedForNativePlacement: (nonnull id) placement;

/**
 * Specify the view controller for a native ad.
 *
 * The view controller can be used to display full screen ads after the user
 * selected a view.
 */
+ (void) setViewController: (nullable UIViewController*) con forNativeAd: (nonnull id) nativeAd;

/**
 * Specify the tracking view for a native ad.
 *
 * The tracking view will be used by the SDK to determine the duration of
 * an ad's visibility.
 */
+ (void) setTrackingView: (null_unspecified UIView*) trackingView forNativeAd: (nonnull id) nativeAd;

/**
 * Remove the tracking view for from native ad.
 *
 * Use this method to remove the tracking view from an ad before configuring
 * it as the tracking view of a different ad.
 */
+ (void) removeTrackingViewForNativeAd: (nonnull id) nativeAd;

/**
 * Get the title for the native ad.
 */
+ (nullable NSString*)   getNativeAdTitle: (nonnull id) nativeAd;

/**
 * Get the description for the native ad.
 */
+ (nullable NSString*)   getNativeAdDescription: (nonnull id) nativeAd;

/**
 * Get the URL string for the native ad image.
 */
+ (nullable NSString*)   getNativeAdImageURL: (nonnull id) nativeAd;

/**
 * Get the URL string for the native ad icon.
 */
+ (nullable NSString*)   getNativeAdIconURL: (nonnull id) nativeAd;

/**
 * Get the title for the call to action item.
 */
+ (nullable NSString*)   getNativeAdCallToAction: (nonnull id) nativeAd;

/**
 * Get the URL string for the native ad branding logo.
 */
+ (nullable NSString*)   getNativeAdBrandingLogoURL: (nonnull id) nativeAd;

/**
 * Get the name of the advertizer for a native ad.
 */
+ (nullable NSString*)   getNativeAdAdvertiser: (nonnull id) nativeAd;

/**
 * Get the type of a native ad.
 */
+ (enum AATKitNativeAdType) getNativeAdType: (nonnull id) nativeAd;

/**
 * Get the rating of the product being advertized.
 */
+ (struct AATKitNativeAdRating) getNativeAdRating: (nonnull id) nativeAd;

/**
 * Get the network providing this native ad.
 */
+ (enum AATKitAdNetwork) getNativeAdNetwork: (nonnull id) nativeAd;

/**
 * Check if a native ad is expired.
 *
 * If you implement pre-fetching of native ads, you can use this method
 * (periodically) to check for stale content.
 */
+ (bool) isNativeAdExpired: (nonnull id) nativeAd;

/**
 * Check if a native ad is ready to be displayed.
 */
+ (bool) isNativeAdReady: (nonnull id) nativeAd;

/**
 * Check if a native ad is a video ad.
 */
+ (bool) isNativeAdVideo: (nonnull id) nativeAd;

/// @name Promo Specials

/**
 * Enable display of promotional full screen ads.
 *
 * You have to call this just once.
 */
+ (void) enablePromo  __deprecated_msg("Will be removed in a future release: Use preparePromo and showPromo: instead.");

/**
 * Disable display of promotional full screen ads.
 *
 * Use this call to temporarily disable promo ads when they would be
 * inappropriate.
 */
+ (void) disablePromo __deprecated_msg("Will be removed in a future release: Use preparePromo and showPromo: instead.");

/**
 * Prepare promotional full screen ads.
 *
 * Prepares promotional fullscreen ads (i.e. load and cache) for display but
 * does not display the ads yet.
 *
 * Calling +[AATKit enablePromo] does this automatically. Usually, there is
 * no reason to call this method.
 *
 * @see +[AATKit enablePromo]
 */
+ (void) preparePromo;

/**
 * Show a promotional full screen ad.
 *
 * *NOTE:* Make sure you have called +[AATKit enablePromo] or +[AATKit
 * preparePromo] before calling this.
 */
+ (void) showPromo __deprecated_msg("Will be removed in a future release: Use showPromo: (bool) force instead");

/**
 * Show a promotional full screen ad (if available).
 *
 *
 * *NOTE:* Make sure you have called +[AATKit enablePromo] or +[AATKit
 * preparePromo] before calling this.
 *
 * @param force `YES` to enable the promo to be shown more often than every
 * hour. `NO` to display the promo is every per hour.
 */
+ (bool) showPromo: (bool) force;

/**
 * …
 *
 * Use this only if you are using per-placement view controllers.
 */
+ (void) setPromoViewController:(nullable UIViewController*)viewcon;

// @name Choosing Ad Networks

/**
 * Disable ad networks programmatically.
 *
 * @see +[AATKit reenableAdNetwork:]
 */
+ (void) disableAdNetwork: (enum AATKitAdNetwork) adNetwork;

/**
 * Enable ad networks programmatically.
 *
 * Usually, you don't have to enable ad networks. You can use this method
 * and `+[AATKit disableAdNetwork:]` to implement your own switches for
 * ad networks.
 *
 * @see +[AATKit disableAdNetwork:]
 */
+ (void) reenableAdNetwork: (enum AATKitAdNetwork) adNetwork;

@end
#endif
