//
//  AdContainerView.m
//  MultiSizeBannerDemo
//
//  Created by Michael on 29/08/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "AdContainerView.h"
@interface AdContainerView()
@property (nonatomic, assign) CGSize adViewSize;
@end

@implementation AdContainerView

- (void) updateContainerViewFrame:(CGSize) adViewSize {
    self.adViewSize = adViewSize;
//    [super setNeedsUpdateConstraints];
    [super invalidateIntrinsicContentSize];
}

- (CGSize) intrinsicContentSize {
    return self.adViewSize;
}

@end
