//
//  AdContainerView.h
//  MultiSizeBannerDemo
//
//  Created by Michael on 29/08/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdContainerView : UIView
- (void) updateContainerViewFrame:(CGSize) adViewSize;
@end
