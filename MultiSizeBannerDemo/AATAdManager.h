//
//  AATAdManager.h
//  MultiSizeBannerDemo
//
//  Created by Michael on 26/08/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIView.h>
@protocol AATAdManagerDelegate
- (void)multiSizeBannerAvailableOnView:(UIView*)multiSizeBannerView;
@end

@interface AATAdManager : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, assign) id<AATAdManagerDelegate> delegate;
- (void)updateAdDisplayingViewController:(UIViewController*)vc;
@end
