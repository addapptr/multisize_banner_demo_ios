//
//  AppDelegate.h
//  MultiSizeBannerDemo
//
//  Created by Michael on 26/08/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

