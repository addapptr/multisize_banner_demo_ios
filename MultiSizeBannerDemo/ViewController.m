//
//  ViewController.m
//  MultiSizeBannerDemo
//
//  Created by Michael on 26/08/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "ViewController.h"
#import "AATAdManager.h"
#import "AdContainerView.h"
@interface ViewController () <AATAdManagerDelegate>
@property (nonatomic, strong) AATAdManager *adManager;
@property (weak, nonatomic) IBOutlet AdContainerView *adContainerView;
@property (weak, nonatomic) UIView *currentlyShownAdView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.adManager = [AATAdManager sharedInstance];
    self.adManager.delegate = self;
    [self.adManager updateAdDisplayingViewController:self];
}

- (CGPoint) calcMidScreenPosToBottomCenterForView:(UIView*) adView {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"multi-size banner view: %@", adView);
    CGPoint midScreenPos = CGPointMake((CGRectGetWidth(self.view.frame) - CGRectGetWidth(adView.frame))/2,
                                       CGRectGetHeight(self.view.frame) - CGRectGetHeight(adView.frame));
    NSLog(@"point to center: %@", NSStringFromCGPoint(midScreenPos));
    return midScreenPos;
}


- (void) multiSizeBannerAvailableOnView:(UIView *)multiSizeBannerView {
    UIView *oldAdView = self.currentlyShownAdView;
    self.currentlyShownAdView = multiSizeBannerView;
    CGPoint pos2CenterView = [self calcMidScreenPosToBottomCenterForView:multiSizeBannerView];

    // position new multi-size banner view outside of the
    multiSizeBannerView.frame = CGRectMake(pos2CenterView.x-CGRectGetWidth(self.view.frame),
                                           CGRectGetHeight(self.adContainerView.bounds) - CGRectGetHeight(multiSizeBannerView.bounds),
                                           CGRectGetWidth(multiSizeBannerView.frame),
                                           CGRectGetHeight(multiSizeBannerView.frame));

    [self.adContainerView addSubview:multiSizeBannerView];
    // position multi-size banner in the center of the screen.
    NSLog(@"Multi-Size banner pos: %@", NSStringFromCGRect(multiSizeBannerView.frame));

    // create frame for multi-size banner container view
    NSLog(@"Multi-Size banner Container pos: %@", NSStringFromCGRect(self.adContainerView.frame));

    // Make sure child views are layouted before the animation.
    [self.currentlyShownAdView setNeedsLayout];
    [self.currentlyShownAdView layoutIfNeeded];

    [UIView animateWithDuration:2 delay:0.0 usingSpringWithDamping:1 initialSpringVelocity:0.0 options:kNilOptions animations:^{
        // resize/grow the container view
        [self resizeAdView];
        [self.adContainerView.superview setNeedsLayout];
        [self.adContainerView.superview layoutIfNeeded];

        oldAdView.frame = CGRectMake(CGRectGetWidth(self.view.frame),
                                     CGRectGetHeight(self.adContainerView.bounds) - CGRectGetHeight(oldAdView.bounds),
                                     0,
                                     self.adContainerView.frame.size.height);
        multiSizeBannerView.frame = CGRectMake(pos2CenterView.x,
                                               0,
                                               CGRectGetWidth(multiSizeBannerView.frame),
                                               CGRectGetHeight(multiSizeBannerView.frame));
    } completion:^(BOOL finished) {
        [oldAdView removeFromSuperview];
        [self.adContainerView bringSubviewToFront:self.currentlyShownAdView];
    }];
}

//resize ad view based on received ad.
- (void)resizeAdView{
    [self.adContainerView updateContainerViewFrame:self.currentlyShownAdView.bounds.size];
}

@end
