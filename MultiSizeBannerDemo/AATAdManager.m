//
//  AATAdManager.m
//  MultiSizeBannerDemo
//
//  Created by Michael on 26/08/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "AATAdManager.h"
#import "AATKit.h"

@interface AATAdManager()<AATKitDelegate>
@property (nonatomic, strong) id bannerPlacement;
@end

@implementation AATAdManager
+ (instancetype)sharedInstance{
    static AATAdManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [AATAdManager new];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [AATKit initWithViewController:nil delegate:self andEnableTestModeWithID:149];
        [AATKit debug:YES];
        self.bannerPlacement = [AATKit createPlacementWithName:@"MultiSizeBanner" andType:AATKitBannerMultiSize];
        [self startAutoReloadOnMultiSizeBanner];
    }
    return self;
}

- (void)updateAdDisplayingViewController:(UIViewController*)vc {
    [AATKit setViewController:vc];
}

- (UIView*)getBannerPlacementView {
    return [AATKit getPlacementView:self.bannerPlacement];
}

- (void)startAutoReloadOnMultiSizeBanner {
    NSLog(@"Started autoreload for multi-size banner");
    [AATKit startPlacementAutoReload:self.bannerPlacement];
}

- (void)stopAutoReloadOnMultiSizeBanner {
    [AATKit stopPlacementAutoReload:self.bannerPlacement];
    NSLog(@"stopped autoreload for multi-size banner");
}

- (void)AATKitObtainedAdRules:(bool)fromTheServer {
    NSLog(@"AATKit obtained rules >> ready to load ads");
}

- (void)AATKitHaveAdOnMultiSizeBanner:(id)placement withView:(UIView *)placementView
{
    NSLog(@"Received multi-size banner with view: %@",placementView);
    [self.delegate multiSizeBannerAvailableOnView:[self getBannerPlacementView]];
}
@end
